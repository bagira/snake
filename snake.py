#!/usr/bin/env python

# by Ina Panova <ina.panova.is@gmail.com>
# git - https://bitbucket.org/bagira/snake/

import curses
import pygame, os
from pygame.locals import *
import time
import sys
from random import randint

n = 10
m = 10

up = 0
down = 1
right = 2
left = 3

snake = [[2,1]]
wherenext = [5,5]
snake_direction = right

score = 0

speed = 10

scale = 50
#0 = show only, 1 = gui only, 2 = both
show = 1

c = False

def usage():

	print "Usage: %s [options]\n\n" %sys.argv[0]
	print ("\t-0		- console mode only\n"
	       "\t-1		- GUI mode only (default)\n"
	       "\t-2		- both\n"
	       "\t-s number	- initial speed of the snake (default 10),"
				 " 0 is the fastest\n"
	       "\t-n number	- vertical size of the field (default 10)\n"
	       "\t-m number	- horizontal size of the field (default 10)\n"
	       "\t-h		- this help\n")

def intersect(position):
	for i in range(0,len(snake)):
		if snake[i] == position:
			return True

	return False

def isdead(position):
	if position[0] == 0 or position[0] == n or \
	position[1] == 0 or position[1] == m or intersect(position):
		return True

	return False

def gonext():
	global snake, wherenext, score, speed;
	sn = snake[0]
	if snake_direction == up:
		nextpos = [sn[0]-1, sn[1]]
	elif snake_direction == down:
		nextpos = [sn[0]+1, sn[1]]
	elif snake_direction == right:
		nextpos = [sn[0], sn[1]+1]
	elif snake_direction == left:
		nextpos = [sn[0], sn[1]-1]
	else:
		print "Wrong position"
		curses.endwin()
		exit(1)

	if isdead(nextpos):
		return False

	snake = [nextpos] + snake

	if nextpos == wherenext:
		score = score + 1
		wherenext = getnext()
		if speed > 0:
		    speed = speed - 1
	else:
		snake.pop()

	return True

def getnext():
	ret = [randint(1,n-1), randint(1,m-1)]
	while intersect(ret):
		ret = [randint(1,n-1), randint(1,m-1)]

	return ret

def printborder():
	for j in range(1,m):
		win.move(0,j)
		win.addch('-')
		win.move(n,j)
		win.addch('-')
	win.move(0,0)
	win.addch('B')
	win.move(0,m)
	win.addch('B')

	for i in range (1,n):
		win.move(i,0)
		win.addch('|')
		win.move(i,m)
		win.addch('|')
	win.move(n,0)
	win.addch('B')
	win.move(n,m)
	win.addch('B')

def drawborder():
	game.blit(wall, (0,0))
	game.blit(wall, (m*scale,0))
	game.blit(wall, (0,n*scale))
	game.blit(wall, (m*scale,n*scale))

	for j in range(1,m):
		game.blit(wall, (j*scale,0))
		game.blit(wall, (j*scale,n*scale))

	for i in range (1,n):
		game.blit(wall, (0,i*scale))
		game.blit(wall, (m*scale,i*scale))

def heartbeat():
	global lastupdate

	if lastupdate + speed/20.0 > time.time():
		return False;

	lastupdate = time.time();

	return True

def printscene():
	win.clear()
	for i in range(0, len(snake)):
		win.move(snake[i][0], snake[i][1])
		win.addch('*')

	win.move(wherenext[0], wherenext[1])
	win.addch('X')

	printborder()

	win.refresh()

def drawscene():
	game.fill((255,255,255))

	for i in range(0, len(snake)):
		game.blit(snake_image, (snake[i][1]*scale, snake[i][0]*scale))

	game.blit(cherry, (wherenext[1]*scale, wherenext[0]*scale))

	drawborder()

	pygame.display.flip()

def updatescene():
	global show;
	if show == 0:
		printscene()
	elif show == 1:
		drawscene()
	elif show == 2:
		printscene()
		drawscene()

def getpressed():
	if show == 0 or show == 2:
		#console

		t = win.getch()
		if t != -1:
			if t == ord('w'):
				return up
			elif t == ord('s'):
				return down
			elif t == ord('a'):
				return left
			elif t == ord('d'):
				return right

	if show == 1 or show == 2:
		#drawing
		pygame.event.pump()
		key = pygame.key.get_pressed()
		if key[pygame.K_ESCAPE]:
			exit(0)
		elif key[pygame.K_w]:
			return up
		elif key[pygame.K_s]:
			return down
		elif key[pygame.K_d]:
			return right
		elif key[pygame.K_a]:
			return left

	return -1


if len(sys.argv) > 1:
	for i in range(1, len(sys.argv)):
		if c:
			c = False
			continue
		if sys.argv[i] == "-0":
			show = 0
		elif sys.argv[i] == "-1":
			show = 1
		elif sys.argv[i] == "-2":
			show = 2
		elif sys.argv[i] == "-n":
			if sys.argv[-1]!="-n" and sys.argv[i+1].isdigit() and \
			   sys.argv[i+1]>4:
			   n = int(sys.argv[i+1]) + 2
			   c = True
			else:
			    print "Integer greater than 4 expected after -n"
			    usage()
			    exit(1)
		elif sys.argv[i] == "-m":
			if sys.argv[-1]!="-m" and sys.argv[i+1].isdigit() and \
			   sys.argv[i+1]>4:
			   m = int(sys.argv[i+1]) + 2
			   c = True
			else:
			    print "Integer greater than 4 expected after -m"
			    usage()
			    exit(1)
		elif sys.argv[i]== "-s":
			if sys.argv[-1]!= "-s" and sys.argv[i+1].isdigit():
			    speed = int(sys.argv[i+1])
			    c = True
			else:
			     print "Integer expected after -s"
			     usage()
                             exit(1)
		else:
			usage()
			exit(1)

if show == 0 or show == 2:
	win = curses.initscr()
	curses.noecho()
	curses.cbreak()
	win.nodelay(1)

if show == 1 or show == 2:
	pygame.init()
	pygame.display.set_mode((m*scale+scale, n*scale+scale))
	game = pygame.display.get_surface()

	wall = pygame.transform.scale(pygame.image.load("images/wall.jpg"), (scale,scale))
	cherry = pygame.transform.scale(pygame.image.load("images/cherry.jpg"), (scale,scale))
	snake_image = pygame.transform.scale(pygame.image.load("images/snake.jpg"), (scale,scale))


lastupdate = time.time()

updatescene()

while True:
	t = getpressed()
	if t != -1:
		if t == up:
			if snake_direction != down:
				snake_direction = up
		elif t == down:
			if snake_direction != up:
				snake_direction = down
		elif t == left:
			if snake_direction != right:
				snake_direction = left
		elif t == right:
			if snake_direction != left:
				snake_direction = right
	if heartbeat():
		if not gonext():
			if show==0 or show==2:
			    curses.endwin()
			print "Dead."
			break;
		updatescene()
	time.sleep(0.1)
